#-------------------------------------------------------------------------------
# Name:     Mail Via Outlook
# Purpose:
#
# Author:      Gowtham.S
#
# Created:     22/07/2015
# Copyright:   (c) Gowtham.S 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import win32com.client
def outlook_main(to_add,cc_add,sub,body,html,att):
    try:
        import win32com.client
        olMailItem = 0x0
        obj = win32com.client.Dispatch("Outlook.Application")
        newMail = obj.CreateItem(olMailItem)
        newMail.Subject = str(sub)
        newMail.Body = str(body)
        newMail.To = to_add
        #newMail.CC = cc_add
        try:
            newMail.HTMLBody  = open(html,'r').read()
        except:
            pass;
        att_1 = open(att,'r').read()
        att_2 = att_1.split("|")
        for fil in att_2:
            newMail.Attachments.Add(fil)
        newMail.Send()
        print "Mail Sent Success...."
    except Excption as e:
        print "Mail Error :"+str(e)

if __name__ == '__main__':
    pass;
    #main(to_add,cc_add,sub,html,att)