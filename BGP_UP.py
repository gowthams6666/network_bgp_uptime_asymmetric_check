import time
import os
import sys
import glob
import threading
#import pysnmp_mibs
from pysnmp.entity.rfc3413.oneliner import cmdgen
import csv
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
import email,email.encoders,email.mime.text,email.mime.base,email.mime.application
from email import Encoders
from datetime import datetime, timedelta
import subprocess
time.sleep(30)
debug = 1
try:
    f = open("debug.txt","r")
    tim = f.readline()
    debug = int(tim)
    f.close()
except:
    debug = 1

class snd_mail():
    def __init__(self):
        pass;
    def send(self,fname):
        try:
            subprocess.call('"D:\\G\\Py\\Portable Python 2.7.6.1\\App\\omail_up.bat" '+str('"'+fname+'"'))
        except Exception as e:
            print e

    def send2(self,eee):
        pass;


class get_date_sec():
    def __init__(self):
        pass;
    def run(self,sec):
        sec = timedelta(seconds=int(sec))
        d = datetime(1,1,1) + sec
        print("DAYS:HOURS:MIN:SEC")
        return str(d.day-1)+'Day '+str(d.hour)+'Hour '+str(d.minute)+'Min '+str(d.second)+'Sec'

class pri():
    def __init__(self,print_d):
        self.print_d = print_d
        self.run()
    def run(self):
            if debug  == 1:
                print "DEBUG> "+str(self.print_d)+" <"

class snmp_val():

    def __init__(self):
        cmdGen = cmdgen.CommandGenerator()
        self.cmdGen = cmdGen

    def get_it(self,get_mib,timeout,retries,ip_1,community,auth1):
        try:
            cmdGen = cmdgen.CommandGenerator()
            self.cmdGen = cmdGen
            aaa = auth1.split("$")
            usrname = aaa[0]
            authKey_1 = aaa[1]
            privKey_1 = aaa[2]
            pro_1 = None
            priv_1 = None
            val_2 = []
            remote_device = cmdgen.UdpTransportTarget((ip_1, 161),timeout,retries)
            #auth_cd = cmdgen.CommunityData(community)
            if aaa[0] == 'None':
                usrname = None
            if aaa[1] == 'None':
                authKey_1 = None
            if aaa[2] == 'None':
                privKey_1 = None
            priv_1 = eval(aaa[3])
            pro_1 = eval(aaa[4])
            if len(authKey_1) < 8:
                authKey_1 = authKey_1+authKey_1
            pri("SNMPV3 AUTH > "+ str(aaa))
            pri("KEY > "+str(authKey_1))
            auth_cd = cmdgen.CommunityData("NPCI_CTS_CSMR")
            #auth_cd = cmdgen.UsmUserData('NPCI_CTS_NMSV3','npcinmsnpcinms',privKey=None,privProtocol=priv_1,authProtocol=pro_1)
            #auth_cd = cmdgen.UsmUserData(usrname,str(authKey_1),privKey=None,privProtocol=priv_1,authProtocol=pro_1)
            mib_oid_val = get_mib
            errorIndication, errorStatus, errorIndex, varBinds = self.cmdGen.getCmd(auth_cd,remote_device,mib_oid_val)
            if errorIndication:
                err = "snmp_error_ind "+ str(errorIndication)
                pri(err)
                return None,str(err)
            else:
                if errorStatus:
                    err_stat = '%s at %s' % (errorStatus.prettyPrint(),errorIndex and varBinds[int(errorIndex)-1] or '?')
                    pri(err_stat)
                    return None,str(err_stat)
                else:
                    for name, val in varBinds:
                        pri('%s = %s' % (name.prettyPrint(), val.prettyPrint()))
                        pri("GET Result: "+str(val))
                        try:
                            val_2.append(val.prettyPrint())
                        except:
                            return None
                        pri(val_2)
                        return val_2
        except Exception as e:
            pri("Error : >"+str(e))
            return None,str(e)
        return None

    def get_nxt(self,get_mib1,get_mib2,timeout,retries,ip_1,community,auth1):
        try:
            cmdGen = cmdgen.CommandGenerator()
            self.cmdGen = cmdGen
            aaa = auth1.split("$")
            rst = []
            usrname = aaa[0]
            authKey_1 = aaa[1]
            privKey_1 = aaa[2]
            pro_1 = None
            priv_1 = None
            remote_device = cmdgen.UdpTransportTarget((ip_1, 161),timeout,retries)
            #auth_cd = cmdgen.CommunityData(community)
            if aaa[0] == 'None':
                usrname = None
            if aaa[1] == 'None':
                authKey_1 = None
            if aaa[2] == 'None':
                privKey_1 = None
            priv_1 = eval(aaa[3])
            pro_1 = eval(aaa[4])
            if len(authKey_1) < 8:
                print len(authKey_1)
                authKey_1 = authKey_1+authKey_1
            pri("SNMPV3 AUTH > "+ str(aaa))
            pri("KEY > "+str(authKey_1))
            auth_cd = cmdgen.CommunityData("NPCI_CTS_CSMR")
            #auth_cd = cmdgen.UsmUserData(usrname,str(authKey_1),privKey=None,privProtocol=priv_1,authProtocol=pro_1)
            #auth_cd = cmdgen.UsmUserData('NPCI_CTS_NMSV3','npcinmsnpcinms',privKey=None,privProtocol=priv_1,authProtocol=pro_1)
            errorIndication, errorStatus, errorIndex, varBinds = self.cmdGen.nextCmd(auth_cd,remote_device,get_mib1,get_mib2,)
            if errorIndication:
                err = "snmp_error_ind "+ str(errorIndication)
                pri(err)
                return None,str(err)
            else:
                if errorStatus:
                    err_stat = '%s at %s' % (errorStatus.prettyPrint(),errorIndex and varBinds[int(errorIndex)-1] or '?')
                    pri(err_stat)
                    return None,str(err_stat)
                else:
                    for name, val in varBinds:
                        pri("Result: "+str(val[0]))
                        rst.append(str(val[0]))
                    pri(rst)
                    return "SuperG",rst
        except Exception as e:
            pri(str(e))
            return None,str(e)
        return None

#my = snmp_val()
#goo = '.1.3.6.1.2.1.4.24.4.1.1.30.1.1.0.255.255.255.0.0.10.10.10.10'
#goo = '1.3.6.1.2.1.15.3.1.7'
#my.get_it(goo,1,10,'10.10.10.100','test')
#my.get_nxt(goo,goo,1,10,'10.10.10.300','test')
class step_it():
    def __init__(self):
        pass;
    def get_bgp_peer_ip(self,ip,timeout,retry,community,auth):
        try:
            bgp_ip = []
            my = snmp_val()
            auth_all = auth.split("\n")
            for aut in auth_all:
                bgp_peer = my.get_nxt('1.3.6.1.2.1.15.3.1.7','1.3.6.1.2.1.15.3.1.7',timeout,retry,ip,community,aut)
                if str(bgp_peer).find("time") > -1:
                    return None
                if str(bgp_peer).find("snmp_error_ind") > -1:
                    #continue
                    return None
                if len(bgp_peer) < 1:
                        pri("Get bgp peer unknown Error")
                        return None
                else:
                        if bgp_peer[0] == "SuperG" :
                            for bip in bgp_peer[1]:
                                xx = '1.3.6.1.2.1.15.3.1.7.'
                                val = bip.split(xx)
                                if len(val) > 1:
                                    bgp_ip.append(val[1])
                                    #print "BGP PEER: "+str(val[1])
                            pri("# BGP PEER : "+str(bgp_ip))
                            return bgp_ip
                        else:
                            pri("BGP PEER STEP ERROR")
                            return None
                #REMOVE THIS FOR SNMP v3
                return None
        except Exception as e:
            pri(e)
            return None
        return None
    def check_ip_route(self,get_mib,timeout,retries,ip_1,community,rip,rmask,rgw,auth):
        try:
            my_2 = snmp_val()
            get_mib = get_mib+'.'+rip+'.'+rmask+'.0.'+rgw
            auth_all = auth.split("\n")
            for aut in auth_all:
                chk = my_2.get_it(get_mib,timeout,retries,ip_1,community,aut)
                if str(chk).find("time") > -1:
                    return None
                if str(chk).find("snmp_error_ind") > -1:
                    #continue
                    return None
                try:
                    if str(chk[0]).find(str(rip)) != -1:
                        print("# ROUTE FOUND:"+str(chk[0]))
                        return chk[0]
                    else:
                        return None
                except :
                    return None
                return None
        except Exception as e:
            pri(str(e))
            return None
        return None
    def get_as_number(self,get_mib,timeout,retries,ip_1,community,rip,auth):
        auth_all = auth.split("\n")
        for aut in auth_all:
            try:
                my_2 = snmp_val()
                get_mib_2 = get_mib+'.'+rip
                chk = my_2.get_it(get_mib_2,timeout,retries,ip_1,community,aut)
                if str(chk).find("time") > -1:
                    return None
                if str(chk).find("snmp_error_ind") > -1:
                    #continue
                    return None
                if chk != None and len(chk) > 0:
                    print("# ASNUMBER :"+str(chk))
                    return chk[0]
            except Exception as e:
                pri(str(e))
                return None
        return None
    def get_as_uptime(self,get_mib,timeout,retries,ip_1,community,rip,auth):

        auth_all = auth.split("\n")
        for aut in auth_all:
            try:
                s_to_d = get_date_sec()
                my_2 = snmp_val()
                get_mib_2 = get_mib+'.'+rip
                chk = my_2.get_it(get_mib_2,timeout,retries,ip_1,community,aut)
                if str(chk).find("time") > -1:
                    return None
                if str(chk).find("snmp_error_ind") > -1:
                    return None
                    #continue
                if chk != None and len(chk) > 0:
                    try:
                        return str(s_to_d.run(int(chk[0])))
                    except Exception as e:
                        pri(e)
                        return None
                        pass;
                return None
            except Exception as e:
                pri(str(e))
                return None
        return None
    def get_as_state(self,get_mib,timeout,retries,ip_1,community,rip,auth):
        bgp_stat = {"1": "idle","2" : "connect","3" : "active","4" : "opensent", "5" : "openconfirm","6" : "established" }
        auth_all = auth.split("\n")
        for aut in auth_all:
            try:
                my_2 = snmp_val()
                get_mib_2 = get_mib+'.'+rip
                chk = my_2.get_it(get_mib_2,timeout,retries,ip_1,community,aut)
                if str(chk).find("time") > -1:
                    return None
                if str(chk).find("snmp_error_ind") > -1:
                    return None
                    #continue
                if chk != None and len(chk) > 0:
                    print("# BGP STATUS :"+str(chk))
                    try:
                        return bgp_stat[str(chk[0])]
                    except Exception as e:
                        pri(e)
                        return None
                        pass;
                return None
            except Exception as e:
                pri(str(e))
                return None
        return None

    def get_as_count(self,r_ip,r_msk,get_mib,timeout,retries,ip_1,community,rip,auth):
        auth_all = auth.split("\n")
        for aut in auth_all:
            try:
                cidr_msk = sum([bin(int(x)).count('1') for x in str(r_msk).split('.')])
                my_2 = snmp_val()
                get_mib_2 = get_mib+'.'+r_ip+"."+str(cidr_msk)+"."+rip
                chk = my_2.get_it(get_mib_2,timeout,retries,ip_1,community,aut)
                if str(chk).find("time") > -1:
                    return None
                if str(chk).find("snmp_error_ind") > -1:
                    return None
                    #continue
                if chk != None and len(chk) > 0:
                    print("# ASCOUNT :"+str(chk))
                    chk_val = False
                    chk_val = type(chk) is list
                    if chk_val == True:
                        all_as = str(chk[0])
                        #check retun value is string or hex , if hex it is ASnumber
                        if str(all_as).find("0x") > -1:
                            fst_as = all_as[6:10]
                            try:
                                fst_as = int(fst_as,16)
                            except Exception as e:
                                pri(e)
                                return None
                            print "AS > ",str(fst_as)+":"+str(((len(all_as)-2)/4)-1)
                            return str(fst_as)+":"+str((len(all_as)-2)/4)
                    return None
            except Exception as e:
                pri(str(e))
                return None
        return None

    def get_device_type(self,ip,timeout,retry,community,auth):
        try:
            auth_all = auth.split("\n")
            for aut in auth_all:
                my = snmp_val()
                device_type = my.get_it('1.3.6.1.2.1.1.1.0',timeout,retry,ip,community,aut)
                chk_val = type(device_type) is list
                if chk_val == True:
                    if device_type != None and len(device_type) > 0:
                        if str(device_type).find("m120") > -1:
                            return "M120"
                        else:
                            return "Cisco"
                #NEED TO REMOVE FOR V3
                return None
        except Exception as e:
            pri(e)
            return None


#cc = step_it()
#cc.get_bgp_peer_ip("192.168.204.36",5,1,'NPCI_CTS_NMS')
#cc.check_ip_route('.1.3.6.1.2.1.4.24.4.1.1',5,1,"10.10.10.100",'test','2.2.2.2','255.255.255.255','10.10.10.200')
#cc.check_ip_route('.1.3.6.1.2.1.4.24.4.1.1', 5, 1, '192.168.204.36', 'NPCI_CTS_NMS', '192.168.180.0', '255.255.255.0', '192.168.206.36')
#bb = cc.get_as_number(".1.3.6.1.2.1.15.3.1.9",5,1,"10.10.10.100",'test','10.10.10.200')
##1.3.6.1.2.1.15.6.1.13.9.9.9.0.30.192.168.175.2
#1.3.6.1.2.1.15.6.1.13.192.168.147.32.28.192.168.92.5
#Get device detailes:1.3.6.1.2.1.1.1

class run_me():
    def __init__(self):
        pass;

    def cisco_snmp(self,login_ip,timeout,retry):
        try:
            fp1 = open("SNMP_AUTH.txt","r")
            snmpv3_auth = fp1.read()
            #pri(snmpv3_auth)
            if len(snmpv3_auth) < 1:
                return None
        except Exception as e:
            print "ERROR > SNMP_AUTH.txt File Opening Error: "+str(e)+" <"
            return None
        try:
            all_up = ''
            cc = step_it()
            print ("#LOGIN TO: "+str(login_ip))
            #dev_typ = cc.get_device_type(str(login_ip),timeout,retry,'NPCI_CTS_NMS',snmpv3_auth)
            b_ip = cc.get_bgp_peer_ip(str(login_ip),timeout,retry,'NPCI_CTS_NMS',snmpv3_auth)
            if b_ip != None:
                for g_ip in b_ip:
                        if g_ip != None:
                            get_as = cc.get_as_number(".1.3.6.1.2.1.15.3.1.9",timeout,retry,login_ip,'NPCI_CTS_NMS',str(g_ip),snmpv3_auth)
                            get_as_sta = cc.get_as_state("1.3.6.1.2.1.15.3.1.2",timeout,retry,login_ip,'NPCI_CTS_NMS',str(g_ip),snmpv3_auth)
                            get_as_up = cc.get_as_uptime("1.3.6.1.2.1.15.3.1.16",timeout,retry,login_ip,'NPCI_CTS_NMS',str(g_ip),snmpv3_auth)
                            print "RESULT > ",str(get_as)+"::"+str(get_as_up)
                            all_up = all_up+"</br>"+str("AS:"+str(get_as)+" UPTIME:"+str(get_as_up)+" STATUS:"+str(get_as_sta))

            return all_up
        except Exception as e:
            pri(e)
            return None
    def run_by_thr(self,timeout,retry,outf,abcd):
        print("DEVICE LIST GET FROM INPUT FILE")
        sp_1 = "NA"
        sp_2 = "NA"
        hub_1 ="NA"
        hub_2 = "NA"
        hub_3 = "NA"
        hub_4 = "NA"
        spk_isp_1 = abcd.get("Spoke-ISP-1")
        spk_isp_2 = abcd.get("Spoke-ISP-2")
        bnk_name = abcd.get('BANK-NAME')
        status = abcd.get('STATUS')
        status = "FINE"
        if len(str(spk_isp_1)) > 5:
            sp_1 = self.cisco_snmp(spk_isp_1,timeout,retry)
        if len(str(spk_isp_2)) > 5:
            sp_2 = self.cisco_snmp(spk_isp_2,timeout,retry)

        try:
            try:
                print "ISP-1",str(spk_isp_1)
                print "ISP-2",str(spk_isp_2)
                if len(str(spk_isp_1)) > 5 and len(str(spk_isp_2)) > 5:
                    if sp_1 != None and len(str(sp_1)) > 2:
                        if (str(sp_1)).count("established") < 2:
                            #NO TWO BGP PEER
                            status = 'ISSUE'
                    else:
                        status = 'ISSUE'
                        sp_1 = 'NOT REACHABLE'
                if len(str(spk_isp_2)) > 5 and len(str(spk_isp_1)) > 5:
                    if sp_2 != None and len(str(sp_2)) > 2:
                        if (str(sp_2)).count("established") < 2:
                            status = 'ISSUE'
                    else:
                        status = 'ISSUE'
                        sp_2 = 'NOT REACHABLE'
		if len(str(sp_1)) < 6 and len(str(sp_2)) < 6:
		    status == "ISSUE"
                if status == "ISSUE":
                    issu_file = open(outf+".txt",'ab')
                    issu_file.write(str(bnk_name)+"\t ISSUE \t"+str(spk_isp_1)+"\t"+str(spk_isp_2)+"\t"+str(sp_1)+'\t'+str(sp_2)+"\n")
                    issu_file.close()
            except Exception as e:
                print "ERROR > OUTPUT FILE(BODY) WRITE ERROR 2",+str(e)

            f = open(outf, 'ab')
            writer = csv.writer(f)
            writer.writerow( (spk_isp_1, spk_isp_2,bnk_name,status,sp_1,sp_2) )

        except Exception as e:
            print "ERROR > OUTPUT FILE(BODY) WRITE ERROR",+str(e)+" <"
        finally:
            f.close()

    def start(self,fname,outf):
        chk_all_lst = []
        timeout = 2
        retry = 1
        try:
            abc=None
            f = open(fname,"r")
            ff = csv.DictReader(f)
        except Exception as e:
            print "ERROR > INPUT_FILE READ ERROR",+str(e)+" <"
        try:
            f = open("timeout.txt","r")
            tim = f.readline()
            tim = tim.split(":")
            timeout = tim[0]
            timeout = int(timeout)
            retry = tim[1]
            retry = int(retry)
            f.close()
        except:
            timeout = 2
            retry = 1
        print("TIME OUT: "+str(timeout))
        print("RETRY: "+str(retry))
        print("DEBUG: "+str(debug))
        try:
            out_file = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
            out_file = "OUTPUT_"+out_file + "_Now.csv"
            f = open(outf, 'wb')
            writer = csv.writer(f)
            writer.writerow( ('Spoke-ISP-1', 'Spoke-ISP-2','BANK-NAME', 'STATUS', 'Spoke-ISP-1-AS', 'Spoke-ISP-2-AS') )
        except :
            print sys.exc_traceback.tb_lineno
            print "ERROR > OUTPUT FILE WRITE ERROR"
            print e
            return None
        finally:
            f.close()
        if True :
            for abcd in ff:
                t = threading.Thread(target=self.run_by_thr, args=(timeout,retry,outf,abcd,))
                t.start()
                t.join()



class start_now():
    def _init__():
        pass
    def run_it(self):
        print "\n\t\t ** BGP TRAFFIC FINDER ** \n"
        stop = False
        while stop == False:
            print "===================================================="
            print "1) BGP UPTIME CHECK"
            print "2) CHANGE TIMEOUT AND RETRY"
            print "3) SET DEBUG"
            print "4) EXIT"
            print "===================================================="
            ch = raw_input("\nENTER YOUR CHOISE > ")
            ch = str(ch)
            if ch == '1' :
                fl = raw_input("ENTER FILE LOCATION (ex: d:\\myfile.csv)> ")
                out_file = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
                if not os.path.exists("D:\\BGP_SYM_REPORT\\"+out_file):
                    os.makedirs("D:\\BGP_SYM_REPORT\\"+out_file)
                r = run_me()
                r.start(fl,"D:\\BGP_SYM_REPORT\\"+out_file+"\\"+fl)
                try:
                    all_d = "D:\\BGP_SYM_REPORT\\"+out_file+"\\*.csv"
                    eml = snd_mail()
                    eml.send(all_d)
                except Exception as e:
                    print e
            if ch == '2' :
                try:
                    f = open("timeout.txt","w")
                    f.write(raw_input(" ENTER TIMEOUT AND RETRY (ex: 5:2) >"))
                    f.close()
                    print "\n*TIMEOUT AND RETRY CHANGED\n"
                except:
                    print "\nSETTING TIME OUT FAILED\n"
            if ch == '3':
                try:
                    f = open("debug.txt","w")
                    f.write(raw_input("\nSET DEBUG ex: 1 or 0 >"))
                    f.close()
                    print "\n *DEBUG CHANGED\n"
                except:
                    print "\nDEBUG CHANGE FAILED\n"
            if ch == '4':
                stop = True
            if ch != 1:
                pass;

if len(sys.argv) == 1:
    s = start_now()
    s.run_it()
elif sys.argv[1].find("ALLBGP") != -1:
    Fin = False
    all_file = []
    all_file.append("Chennai_PR.csv")
    all_file.append("Mumbai_PR.csv")
    all_file.append("Delhi_PR.csv")
    out_file = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    if not os.path.exists("D:\\BGP_SYM_REPORT\\"+out_file):
        os.makedirs("D:\\BGP_SYM_REPORT\\"+out_file)
    while Fin == False:
        for fl in all_file:
            try:
                r = run_me()
                r.start(fl,"D:\\BGP_SYM_REPORT\\"+out_file+"\\"+fl)
            except:
                pass;
        if not os.path.exists("D:\\BGP_SYM_REPORT\\"+out_file):
            os.makedirs("D:\\BGP_SYM_REPORT\\"+out_file+"\\REPORT_COMPLETED")
        Fin = True
    try:
        all_d = "D:\\BGP_SYM_REPORT\\"+out_file+"\\*.csv"
        eml = snd_mail()
        eml.send(all_d)
    except Exception as e:
        print "MAIL SENDING ERROR"
        print e

elif sys.argv[1].find("ALLBGP") == -1:
    fff = sys.argv[1]
    Fin = False
    all_file = []
    all_file.append(fff)
    out_file = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    if not os.path.exists("D:\\BGP_SYM_REPORT\\"+out_file):
        os.makedirs("D:\\BGP_SYM_REPORT\\"+out_file)
    while Fin == False:
        for fl in all_file:
            try:
                r = run_me()
                print "D:\\BGP_SYM_REPORT\\"+out_file+"\\"+fl
                r.start(fl,"D:\\BGP_SYM_REPORT\\"+out_file+"\\"+fl)
            except:
                pass;
        if not os.path.exists("D:\\BGP_SYM_REPORT\\"+out_file):
            os.makedirs("D:\\BGP_SYM_REPORT\\"+out_file+"\\REPORT_COMPLETED")
        Fin = True
    try:
        all_d = "D:\\BGP_SYM_REPORT\\"+out_file+"\\*.csv"
        eml = snd_mail()
        eml.send(all_d)
    except Exception as e:
        print "MAIL SENDING ERROR"
        print e