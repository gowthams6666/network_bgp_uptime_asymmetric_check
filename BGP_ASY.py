#1.3.6.1.2.1.4.24.4.1.10 NEX VERSION SOLVE
import time
import datetime
import os
import sys
import glob
import threading
#import pysnmp_mibs
from pysnmp.entity.rfc3413.oneliner import cmdgen
import csv
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
import email,email.encoders,email.mime.text,email.mime.base,email.mime.application
from email import Encoders
import subprocess


class snd_mail():
    def __init__(self):
        pass;
    def send(self,fname):
        try:
            subprocess.call('"D:\\G\\Py\\Portable Python 2.7.6.1\\App\\omail.bat" '+str('"'+fname+'"'))
        except Exception as e:
                print e
    def send2(self,fname):
        pass;

debug = 1
try:
    f = open("debug.txt","r")
    tim = f.readline()
    debug = int(tim)
    f.close()
except:
    debug = 1

class chk_asymentric():
    def __init__(self):
        pass
    def start(self,sp_1,sp_2,hub_1,hub_2,hub_3,hub_4):
        try:
            ######################### FORMATING BASED ON JUNIPER AND CISCO STARTED ##########
            pri("CHECKING ASYMENTRIC")
            sym = ''
            hub_2_count = 1000
            hub_1_count = 1000
            print sp_1,sp_2,hub_1,hub_2,hub_3,hub_4
            sym1 = 'NOT_SYMENTRIC'
            sym2 = 'NOT_SYMENTRIC'
            try:
                hub_1 = hub_1.split(":")
            except :
                pass;
            if hub_1 != None and  len(hub_1) > 1:
                hub_1_count = hub_1[1]
                hub_1 = hub_1[0]
                hub_1_count = int(hub_1_count)
            elif hub_1 != None:
                hub_1 = hub_1[0]
            try:
                hub_2 = hub_2.split(":")
            except:
                pass;
            if hub_2 != None and len(hub_2) > 1:
                hub_2_count = hub_2[1]
                hub_2 = hub_2[0]
                hub_2_count = int(hub_2_count)
            elif hub_2 != None:
                hub_2 = hub_2[0]
            #JUNIPER ROUTER
            print "HUB1,2count.....",hub_1_count,hub_2_count,hub_1,hub_2
            if hub_1_count != 1000 or hub_2_count != 1000:
                #JUNIPER ROUTER
                if hub_1_count > hub_2_count:
                    hub_1 = ''
                if hub_2_count > hub_1_count:
                    hub_2 = ''
            ######################### FORMATED BASED ON JUNIPER AND CISCO COMPLETED ##########
            ######################### CHECKING SYMENTRIC TRAFFIC #############################
            sym = "ASYMENTRIC"
            if sp_1 == hub_1 or sp_2 == hub_1:
                sym1 = "SYMENTRIC"
            if sp_1 == hub_2 or sp_2 == hub_2:
                sym2 = "SYMENTRIC"
            if sym1 == "SYMENTRIC" or sym2 == "SYMENTRIC":
                sym = 'SYMENTRIC'
            h1_s = type(hub_1) is list
            h2_s = type(hub_2) is list
            if h1_s == True:
                hub_1 = hub_1[0]
            if h2_s == True:
                hub_2 = hub_2[0]
            pri("CHECKING ASYMENTRIC DONE")
            if sp_1 == None:
                sp_1 = "TIME OUT"
                sym = "INCOMPLETE"
            if sp_2 == None:
                sp_2 = "TIME OUT"
                sym = "INCOMPLETE"
            if hub_1 == None:
                hub_1 = "TIME OUT"
                sym = "INCOMPLETE"
            if hub_2 == None:
                hub_2 = "TIME OUT"
                sym = "INCOMPLETE"
            if hub_3 == None:
                hub_3 = "TIME OUT"
                sym = "INCOMPLETE"
            if hub_4 == None:
                hub_4 = "TIME OUT"
                sym = "INCOMPLETE"
            return sp_1,sp_2,hub_1,hub_2,hub_3,hub_4,sym
        except Exception as e:
            pri('chk_asymentric'+str(e))
            pri("CHECKING ASYMENTRIC DONE WITH EXCEPTION")
            return sp_1,sp_2,hub_1,hub_2,hub_3,hub_4,"CHECK MANUALY"


class pri():
    def __init__(self,print_d):
        self.print_d = print_d
        self.run()
    def run(self):
            if debug  == 1:
                print "DEBUG> "+str(self.print_d)+" <"

class snmp_val():

    def __init__(self):
        cmdGen = cmdgen.CommandGenerator()
        self.cmdGen = cmdGen

    def get_it(self,get_mib,timeout,retries,ip_1,community,auth1):
        try:
            cmdGen = cmdgen.CommandGenerator()
            self.cmdGen = cmdGen
            aaa = auth1.split("$")
            usrname = aaa[0]
            authKey_1 = aaa[1]
            privKey_1 = aaa[2]
            pro_1 = None
            priv_1 = None
            val_2 = []
            remote_device = cmdgen.UdpTransportTarget((ip_1, 161),timeout,retries)
            #auth_cd = cmdgen.CommunityData(community)
            if aaa[0] == 'None':
                usrname = None
            if aaa[1] == 'None':
                authKey_1 = None
            if aaa[2] == 'None':
                privKey_1 = None
            priv_1 = eval(aaa[3])
            pro_1 = eval(aaa[4])
            if len(authKey_1) < 8:
                authKey_1 = authKey_1+authKey_1
            pri("SNMPV3 AUTH > "+ str(aaa))
            pri("KEY > "+str(authKey_1))
            #auth_cd = cmdgen.UsmUserData('NPCI_CTS_NMSV3','npcinmsnpcinms',privKey=None,privProtocol=priv_1,authProtocol=pro_1)
            auth_cd = cmdgen.CommunityData("NPCI_CTS_CSMR")
            #auth_cd = cmdgen.UsmUserData(usrname,str(authKey_1),privKey=None,privProtocol=priv_1,authProtocol=pro_1)
            mib_oid_val = get_mib
            errorIndication, errorStatus, errorIndex, varBinds = self.cmdGen.getCmd(auth_cd,remote_device,mib_oid_val)
            if errorIndication:
                err = "snmp_error_ind "+ str(errorIndication)
                pri(err)
                return None,str(err)
            else:
                if errorStatus:
                    err_stat = '%s at %s' % (errorStatus.prettyPrint(),errorIndex and varBinds[int(errorIndex)-1] or '?')
                    pri(err_stat)
                    return None,str(err_stat)
                else:
                    for name, val in varBinds:
                        pri('%s = %s' % (name.prettyPrint(), val.prettyPrint()))
                        pri("GET Result: "+str(val))
                        try:
                            val_2.append(val.prettyPrint())
                        except:
                            return None
                        pri(val_2)
                        return val_2
        except Exception as e:
            pri("Error : >"+str(e))
            return None,str(e)
        return None

    def get_nxt(self,get_mib1,get_mib2,timeout,retries,ip_1,community,auth1):
        try:
            cmdGen = cmdgen.CommandGenerator()
            self.cmdGen = cmdGen
            aaa = auth1.split("$")
            rst = []
            usrname = aaa[0]
            authKey_1 = aaa[1]
            privKey_1 = aaa[2]
            pro_1 = None
            priv_1 = None
            remote_device = cmdgen.UdpTransportTarget((ip_1, 161),timeout,retries)
            #auth_cd = cmdgen.CommunityData(community)
            if aaa[0] == 'None':
                usrname = None
            if aaa[1] == 'None':
                authKey_1 = None
            if aaa[2] == 'None':
                privKey_1 = None
            priv_1 = eval(aaa[3])
            pro_1 = eval(aaa[4])
            if len(authKey_1) < 8:
                print len(authKey_1)
                authKey_1 = authKey_1+authKey_1
            pri("SNMPV3 AUTH > "+ str(aaa))
            pri("KEY > "+str(authKey_1))
            #auth_cd = cmdgen.UsmUserData(usrname,str(authKey_1),privKey=None,privProtocol=priv_1,authProtocol=pro_1)
            #auth_cd = cmdgen.UsmUserData('NPCI_CTS_NMSV3','npcinmsnpcinms',privKey=None,privProtocol=priv_1,authProtocol=pro_1)
            auth_cd = cmdgen.CommunityData("NPCI_CTS_CSMR")
            errorIndication, errorStatus, errorIndex, varBinds = self.cmdGen.nextCmd(auth_cd,remote_device,get_mib1,get_mib2,)
            if errorIndication:
                err = "snmp_error_ind "+ str(errorIndication)
                pri(err)
                return None,str(err)
            else:
                if errorStatus:
                    err_stat = '%s at %s' % (errorStatus.prettyPrint(),errorIndex and varBinds[int(errorIndex)-1] or '?')
                    pri(err_stat)
                    return None,str(err_stat)
                else:
                    for name, val in varBinds:
                        pri("Result: "+str(val[0]))
                        rst.append(str(val[0]))
                    pri(rst)
                    return "SuperG",rst
        except Exception as e:
            pri(str(e))
            return None,str(e)
        return None

#my = snmp_val()
#goo = '.1.3.6.1.2.1.4.24.4.1.1.30.1.1.0.255.255.255.0.0.10.10.10.10'
#goo = '1.3.6.1.2.1.15.3.1.7'
#my.get_it(goo,1,10,'10.10.10.100','test')
#my.get_nxt(goo,goo,1,10,'10.10.10.300','test')
class step_it():
    def __init__(self):
        pass;
    def get_bgp_peer_ip(self,ip,timeout,retry,community,auth):
        try:
            bgp_ip = []
            my = snmp_val()
            auth_all = auth.split("\n")
            for aut in auth_all:
                bgp_peer = my.get_nxt('1.3.6.1.2.1.15.3.1.7','1.3.6.1.2.1.15.3.1.7',timeout,retry,ip,community,aut)
                if str(bgp_peer).find("time") > -1:
                    return None
                if str(bgp_peer).find("snmp_error_ind") > -1:
                    return None
                    continue
                if len(bgp_peer) < 1:
                        pri("Get bgp peer unknown Error")
                        return None
                else:
                        if bgp_peer[0] == "SuperG" :
                            for bip in bgp_peer[1]:
                                xx = '1.3.6.1.2.1.15.3.1.7.'
                                val = bip.split(xx)
                                if len(val) > 1:
                                    bgp_ip.append(val[1])
                                    #print "BGP PEER: "+str(val[1])
                            pri("# BGP PEER : "+str(bgp_ip))
                            return bgp_ip
                        else:
                            pri("BGP PEER STEP ERROR")
                            return None
                return None
        except Exception as e:
            pri(e)
            return None
        return None
    def check_ip_route(self,get_mib,timeout,retries,ip_1,community,rip,rmask,rgw,auth):
        try:
            my_2 = snmp_val()
            get_mib = get_mib+'.'+rip+'.'+rmask+'.0.'+rgw
            auth_all = auth.split("\n")
            for aut in auth_all:
                chk = my_2.get_it(get_mib,timeout,retries,ip_1,community,aut)
                if str(chk).find("time") > -1:
                    return None
                if str(chk).find("snmp_error_ind") > -1:
                    return None
                    continue
                try:
                    if str(chk[0]).find(str(rip)) != -1:
                        print("# ROUTE FOUND:"+str(chk[0]))
                        return chk[0]
                    else:
                        return None
                except :
                    return None
                return None
        except Exception as e:
            pri(str(e))
            return None
        return None
    def get_as_number(self,get_mib,timeout,retries,ip_1,community,rip,auth):
        auth_all = auth.split("\n")
        for aut in auth_all:
            try:
                my_2 = snmp_val()
                get_mib_2 = get_mib+'.'+rip
                chk = my_2.get_it(get_mib_2,timeout,retries,ip_1,community,aut)
                if str(chk).find("time") > -1:
                    return None
                if str(chk).find("snmp_error_ind") > -1:
                    return None
                    continue
                if chk != None and len(chk) > 0:
                    print("# ASNUMBER :"+str(chk))
                    return chk[0]
            except Exception as e:
                pri(str(e))
                return None
            return None
        return None

    def get_as_count(self,r_ip,r_msk,get_mib,timeout,retries,ip_1,community,rip,auth):
        auth_all = auth.split("\n")
        for aut in auth_all:
            try:
                cidr_msk = sum([bin(int(x)).count('1') for x in str(r_msk).split('.')])
                my_2 = snmp_val()
                get_mib_2 = get_mib+'.'+r_ip+"."+str(cidr_msk)+"."+rip
                chk = my_2.get_it(get_mib_2,timeout,retries,ip_1,community,aut)
                if str(chk).find("time") > -1:
                    return None
                if str(chk).find("snmp_error_ind") > -1:
                    return None
                    continue
                if chk != None and len(chk) > 0:
                    print("# ASCOUNT :"+str(chk))
                    chk_val = False
                    chk_val = type(chk) is list
                    if chk_val == True:
                        all_as = str(chk[0])
                        #check retun value is string or hex if hex it is ASnumber
                        if str(all_as).find("0x") > -1:
                            fst_as = all_as[6:10]
                            try:
                                fst_as = int(fst_as,16)
                            except Exception as e:
                                pri(e)
                                return None
                            print "AS > ",str(fst_as)+":"+str(((len(all_as)-2)/4)-1)
                            return str(fst_as)+":"+str((len(all_as)-2)/4)
                        return None
                return None
            except Exception as e:
                pri(str(e))
                return None
        return None

    def get_device_type(self,ip,timeout,retry,community,auth):
        try:
            auth_all = auth.split("\n")
            for aut in auth_all:
                my = snmp_val()
                device_type = my.get_it('1.3.6.1.2.1.1.1.0',timeout,retry,ip,community,aut)
                chk_val = type(device_type) is list
                if chk_val == True:
                    if device_type != None and len(device_type) > 0:
                        if str(device_type).find("m120") > -1:
                            return "M120"
                        else:
                            return "Cisco"
                return None
        except Exception as e:
            pri(e)
            return None


#cc = step_it()
#cc.get_bgp_peer_ip("192.168.204.36",5,1,'NPCI_CTS_NMS')
#cc.check_ip_route('.1.3.6.1.2.1.4.24.4.1.1',5,1,"10.10.10.100",'test','2.2.2.2','255.255.255.255','10.10.10.200')
#cc.check_ip_route('.1.3.6.1.2.1.4.24.4.1.1', 5, 1, '192.168.204.36', 'NPCI_CTS_NMS', '192.168.180.0', '255.255.255.0', '192.168.206.36')
#bb = cc.get_as_number(".1.3.6.1.2.1.15.3.1.9",5,1,"10.10.10.100",'test','10.10.10.200')
#1.3.6.1.2.1.15.6.1.13.9.9.9.0.30.192.168.175.2
#1.3.6.1.2.1.15.6.1.13.192.168.147.32.28.192.168.92.5
#Get device detailes:1.3.6.1.2.1.1.1

class run_me():
    def __init__(self):
        pass;

    def cisco_snmp(self,login_ip,route_ip,route_mask,timeout,retry):
        try:
            fp1 = open("SNMP_AUTH.txt","r")
            snmpv3_auth = fp1.read()
            #pri(snmpv3_auth)
            if len(snmpv3_auth) < 1:
                return None
        except Exception as e:
            print "ERROR > SNMP_AUTH.txt File Opening Error: "+str(e)+" <"
            return None
        try:
            cc = step_it()
            print ("#LOGIN TO: "+str(login_ip))
            dev_typ = cc.get_device_type(str(login_ip),timeout,retry,'NPCI_CTS_NMS',snmpv3_auth)
            b_ip = cc.get_bgp_peer_ip(str(login_ip),timeout,retry,'NPCI_CTS_NMS',snmpv3_auth)
            if b_ip != None:
                for g_ip in b_ip:
                    #IF DEVICE IS JUNIPER M120
                    if dev_typ == "M120":
                        pri("Device vendor: Juniper")
                        get_rt = cc.get_as_count(route_ip,route_mask,".1.3.6.1.2.1.15.6.1.5",timeout,retry,login_ip,'NPCI_CTS_NMS',str(g_ip),snmpv3_auth)
                        if get_rt != None:
                            return get_rt
                    #IF DEVICE IS CISCO
                    else:
                        pri("Device vendor: Cisco")
                        get_rt = cc.check_ip_route('.1.3.6.1.2.1.4.24.4.1.1',timeout,retry,login_ip,'NPCI_CTS_NMS',route_ip,route_mask,g_ip,snmpv3_auth)
                        pri("IP ROUTE STATUS"+str(get_rt))
                        if get_rt != None:
                            get_as = cc.get_as_number(".1.3.6.1.2.1.15.3.1.9",timeout,retry,login_ip,'NPCI_CTS_NMS',str(g_ip),snmpv3_auth)
                            return get_as
        except Exception as e:
            pri(e)
            return None
    def run_by_thr(self,timeout,retry,outf,abcd):
        print("DEVICE LIST GET FROM INPUT FILE")
        sp_1 = "NA"
        sp_2 = "NA"
        hub_1 ="NA"
        hub_2 = "NA"
        hub_3 = "NA"
        hub_4 = "NA"
        spk_isp_1 = abcd.get("Spoke-ISP-1")
        spk_isp_2 = abcd.get("Spoke-ISP-2")
        hub_isp_1 = abcd.get('Hub-ISP-1')
        hub_isp_2 = abcd.get('Hub-ISP-2')
        hub_isp_3 = abcd.get('Hub-ISP-3')
        hub_isp_4 = abcd.get('Hub-ISP-4')
        bnk_name = abcd.get('BANK-NAME')
        status = abcd.get('STATUS')
        spoke_route = abcd.get('Spoke-Route')
        hub_route = abcd.get('Hub-Route')
        spoke_mask = abcd.get('Spoke-Mask')
        hub_mask = abcd.get('Hub-Mask')
        if len(str(spk_isp_1)) > 5:
            sp_1 = self.cisco_snmp(spk_isp_1,hub_route,hub_mask,timeout,retry)
        if len(str(spk_isp_2)) > 5:
            sp_2 = self.cisco_snmp(spk_isp_2,hub_route,hub_mask,timeout,retry)
        if len(str(hub_isp_1)) > 5:
            hub_1 = self.cisco_snmp(hub_isp_1,spoke_route,spoke_mask,timeout,retry)
        if len(str(hub_isp_2)) > 5:
            hub_2 = self.cisco_snmp(hub_isp_2,spoke_route,spoke_mask,timeout,retry)
        if len(str(hub_isp_3)) > 5:
            hub_3 = self.cisco_snmp(hub_isp_3,spoke_route,spoke_mask,timeout,retry)
        if len(str(hub_isp_4)) > 5:
            hub_4 = self.cisco_snmp(hub_isp_4,spoke_route,spoke_mask,timeout,retry)
        try:
            f = open(outf, 'ab')
            writer = csv.writer(f)
            chk_as = chk_asymentric()
            print hub_2
            sp_1,sp_2,hub_1,hub_2,hub_3,hub_4,status = chk_as.start(sp_1,sp_2,hub_1,hub_2,hub_3,hub_4)
            writer.writerow( (spk_isp_1, spk_isp_2,hub_isp_1,hub_isp_2,hub_isp_3,hub_isp_4,spoke_route,spoke_mask,hub_route,hub_mask,bnk_name,status,sp_1,sp_2,hub_1,hub_2,hub_3,hub_4) )
        except Exception as e:
            print "ERROR > OUTPUT FILE(BODY) WRITE ERROR",+str(e)+" <"
        finally:
            f.close()
        try:
            ff = open(outf+".txt", 'ab')
            if status != "SYMENTRIC":
                ff.write(str(bnk_name + "\t"+ spk_isp_1 + "\t"+ spk_isp_2 + "\t"+ status + "\t"+ sp_1 + "\t"+ sp_2 + "\t"+ hub_1 + "\t"+ hub_2 + "\r\n"))
        except Exception as e:
            pri(str(e))
        finally:
            ff.close()

    def start(self,fname,outf):
        chk_all_lst = []
        timeout = 4
        retry = 2
        try:
            print "STARTED"
            abc=None
            f = open(fname,"r")
            ff = csv.DictReader(f)
        except Exception as e:
            print "ERROR > INPUT_FILE READ ERROR",+str(e)+" <"
        try:
            f = open("timeout.txt","r")
            tim = f.readline()
            tim = tim.split(":")
            timeout = tim[0]
            timeout = int(timeout)
            retry = tim[1]
            retry = int(retry)
            f.close()
        except:
            timeout = 2
            retry = 1
        print("TIME OUT: "+str(timeout))
        print("RETRY: "+str(retry))
        print("DEBUG: "+str(debug))
        try:
            out_file = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
            out_file = "OUTPUT_"+out_file + "_Now.csv"
            f = open(outf, 'wb')
            writer = csv.writer(f)
            writer.writerow( ('Spoke-ISP-1', 'Spoke-ISP-2', 'Hub-ISP-1', 'Hub-ISP-2', 'Hub-ISP-3', 'Hub-ISP-4', 'Spoke-Route', 'Spoke-Mask', 'Hub-Route', 'Hub-Mask', 'BANK-NAME', 'STATUS', 'Spoke-ISP-1-AS', 'Spoke-ISP-2-AS', 'Hub-ISP-1-AS', 'Hub-ISP-2-AS', 'Hub-ISP-3-AS', 'Hub-ISP-4-AS') )
        except Exception as e:
            print "ERROR > OUTPUT FILE WRITE ERROR",+str(e)+" <"
            return None
        finally:
            f.close()
        if True :
            for abcd in ff:
                t = threading.Thread(target=self.run_by_thr, args=(timeout,retry,outf,abcd,))
                t.start()
                t.join(90)
        try:
            f.close()
        except:
            pass;

class start_now():
    def _init__():
        pass
    def run_it(self):
        print "\n\t\t ** BGP TRAFFIC FINDER ** \n"
        stop = False
        while stop == False:
            print "===================================================="
            print "1) SYMENTRIC TRAFFIC CHECK"
            print "2) CHANGE TIMEOUT AND RETRY"
            print "3) SET DEBUG"
            print "4) EXIT"
            print "===================================================="
            ch = raw_input("\nENTER YOUR CHOISE > ")
            ch = str(ch)
            if ch == '1' :
                fl = raw_input("ENTER FILE LOCATION (ex: d:\\myfile.csv)> ")
                out_file = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
                if not os.path.exists("D:\\BGP_SYM_REPORT\\"+out_file):
                    os.makedirs("D:\\BGP_SYM_REPORT\\"+out_file)
                r = run_me()
                r.start(fl,"D:\\BGP_SYM_REPORT\\"+out_file+"\\"+fl)
            if ch == '2' :
                try:
                    f = open("timeout.txt","w")
                    f.write(raw_input(" ENTER TIMEOUT AND RETRY (ex: 5:2) >"))
                    f.close()
                    print "\n*TIMEOUT AND RETRY CHANGED\n"
                except:
                    print "\nSETTING TIME OUT FAILED\n"
            if ch == '3':
                try:
                    f = open("debug.txt","w")
                    f.write(raw_input("\nSET DEBUG ex: 1 or 0 >"))
                    f.close()
                    print "\n *DEBUG CHANGED\n"
                except:
                    print "\nDEBUG CHANGE FAILED\n"
            if ch == '4':
                stop = True
            if ch != 1:
                pass;

if len(sys.argv) == 1:
    s = start_now()
    s.run_it()
elif sys.argv[1].find("ALLBGP") != -1:
    Fin = False
    all_file = []
    all_file.append("Chennai_pr.csv")
    all_file.append("Chennai_dr.csv")
    all_file.append("Mumbai_pr.csv")
    all_file.append("Mumbai_dr.csv")
    all_file.append("Delhi_pr.csv")
    all_file.append("Delhi_dr.csv")
    out_file = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    if not os.path.exists("D:\\BGP_SYM_REPORT\\"+out_file):
        os.makedirs("D:\\BGP_SYM_REPORT\\"+out_file)
    while Fin == False:
        for fl in all_file:
            try:
                r = run_me()
                r.start(fl,"D:\\BGP_SYM_REPORT\\"+out_file+"\\"+fl)
            except:
                pass;
        if not os.path.exists("D:\\BGP_SYM_REPORT\\"+out_file):
            os.makedirs("D:\\BGP_SYM_REPORT\\"+out_file+"\\REPORT_COMPLETED")
        Fin = True
    try:
        all_d = "D:\\BGP_SYM_REPORT\\"+out_file+"\\*.csv"
        eml = snd_mail()
        eml.send(all_d)
    except Exception as e:
        print "MAIL SENDING ERROR"
        print e

elif sys.argv[1].find("ALLBGP") == -1:
    fff = sys.argv[1]
    Fin = False
    all_file = []
    all_file.append(fff)
    out_file = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    if not os.path.exists("D:\\BGP_SYM_REPORT\\"+out_file):
        os.makedirs("D:\\BGP_SYM_REPORT\\"+out_file)
    while Fin == False:
        for fl in all_file:
            try:
                r = run_me()
                print "D:\\BGP_SYM_REPORT\\"+out_file+"\\"+fl
                r.start(fl,"D:\\BGP_SYM_REPORT\\"+out_file+"\\"+fl)
            except:
                pass;
        if not os.path.exists("D:\\BGP_SYM_REPORT\\"+out_file):
            os.makedirs("D:\\BGP_SYM_REPORT\\"+out_file+"\\REPORT_COMPLETED")
        Fin = True
    try:
        all_d = "D:\\BGP_SYM_REPORT\\"+out_file+"\\*.csv"
        eml = snd_mail()
        eml.send(all_d)
    except Exception as e:
        print "MAIL SENDING ERROR"
        print e
